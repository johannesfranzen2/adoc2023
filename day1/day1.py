lookup = {"one":"1",
"two":"2",
"three":"3",
"four":"4",
"five":"5",
"six":"6",
"seven":"7",
"eight":"8",
"nine":"9"}


def find_numbers(data):
    num = []    
    for i,d in enumerate(data):
        try:
            if d.isdigit():
                num.append(d)
                continue     
                   
        #Check for the strings
        #Check for len 3 letters
        
            if data[i:i+3] in lookup:
                num.append(lookup[data[i:i+3]])
                continue
            #Check for len 4 letters
            elif data[i:i+4] in lookup:
                num.append(lookup[data[i:i+4]])
                continue
            #Check for len 5 letters
            elif data[i:i+5] in lookup:
               num.append(lookup[data[i:i+5]])
               continue
        except Exception as e:
            print(e)    
            
    return int(num[0]+num[-1])

with open("input2","r") as file:
    lines = file.readlines()

c=0
for line in lines:
    c+= find_numbers(line)    

print(c)