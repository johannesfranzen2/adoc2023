with open("day4/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]

def parse_input(lines):
    w = []
    p = []
    for line in lines:    
        winner,player = line.split("|")    
        _, winner = winner.split(":")
        w.append([num for num in winner.split(" ") if not num == ""])
        p.append([num for num in player.split(" ") if not num == ""])
    return (w,p)

winner, player = parse_input(lines)
total_score = 0

for win,play in zip(winner,player):    
    l = len(win) + len(play)
    unique = set(win)
    unique.update(set(play))
    c = pow(2,l-len(unique)-1)
    if c >=1:
        total_score += c    

print(total_score)
