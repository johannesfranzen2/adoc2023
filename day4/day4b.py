with open("day4/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]

count_cards = {k:1 for k in range(1,201+1)}

for card_n,line in enumerate(lines,start=1):
    matching_numbers=0
    winner,player = line.split("|")    
    _, winner = winner.split(":")
    winner = [num for num in winner.split(" ") if not num == ""]
    player = [num for num in player.split(" ") if not num == ""]   
    
    for number in player:
        if number in winner:
            matching_numbers+=1

    #We now know X matching numbers
    for i in range(card_n+1,card_n+matching_numbers+1):
        count_cards[i]+=(count_cards[card_n])

print(sum(count_cards.values()))
