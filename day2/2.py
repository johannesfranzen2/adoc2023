class Combo():
    def __init__(self, entry):
        self.ID = int(entry["ID"])
        self.red = entry["red"]
        self.blue = entry["blue"]
        self.green = entry["green"]

    def __lt__(self, comp):           
          if self.red < comp.red or self.blue < comp.blue or self.green < comp.green:
              return False        
          return True  

def get_entry_amounts(line:list) -> dict:           
    temp_dict = {}
    m = line.split()        
    temp_dict["ID"] = m[1].strip(":") #always on index 2        
    numbers = m[2:] #Slice out the remainder

    for i in range(0,len(numbers),2):
        v = int(numbers[i].strip(",;"))
        k = numbers[i+1].strip(",;")
        try: 
            if temp_dict[k] < v:
                temp_dict[k] = v
        except KeyError:
            temp_dict[k] = v               

    return Combo(temp_dict)

    

elf = Combo({"ID":0, "red": 12, "blue": 14, "green":13})

with open("input2a_real", "r") as file:    
    lines = file.readlines()        

score = 0
highest_score = 0
for line in lines:
     if line != None: 
        entry = get_entry_amounts(line)
        if  entry > elf:
            score+=entry.ID

        highest_score += (entry.red * entry.blue * entry.green)

print(f"Score: {score}")
print(f"highest Score: {highest_score}")