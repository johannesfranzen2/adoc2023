with open("day11/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]        
    
#Expand
#Find Galaxy cordinates
#Take 1st Galaxy and check against every other galaxy lower in the list
#Cal X, Y delta
#Count
    

#PArt 2 is unsolved. Might revisit

import numpy as np

class Universe():
    def __init__(self, raw, modifier=1000000):
        self.raw = raw
        self.parse_uni()
        self.find_cordinates()
        self.find_combos()
        self.calc_delta()

    def parse_uni(self):
        #Expand X
        rows = []
        for row in self.raw:
            rows.append(row)
            if len(set(row)) == 1:
                rows.append(row)
        
        #Expand Y        
        self.matrix = np.vstack([list(row) for row in rows])      
        # Accessing entire rows or columns
        #first_row = matrix[0, :]  # Entire first row
        #second_column = matrix[:, 1]  # Entire second column

        new_matrix = self.matrix
        c=0
        for i in range(0,len(self.matrix[0, :])):
            if len(set(self.matrix[:, i])) == 1:                             
                new_matrix = np.insert(new_matrix, i+c, self.matrix[:, i], axis=1)
                c+=1

        self.matrix = new_matrix        

    def find_cordinates(self):    
        cordinates = []    
        for x,row in enumerate(self.matrix):
            for y,col in enumerate(self.matrix.T):
                if self.matrix[x,y] == "#":
                   cordinates.append((x,y))

        self.cordinates = cordinates

    def find_combos(self):
        
        combinations = []
        for i,cord in enumerate(self.cordinates):            
            for i_ in range(i, len(self.cordinates)):
                if i_+1== len(self.cordinates):
                    break
                combinations.append((self.cordinates[i_+1], cord))        

        self.combinations = combinations

    def calc_delta(self):
        c = 0
        for a,b in self.combinations:
            x1,y1 = a
            x2,y2 = b
            
            delta = abs(x1-x2) + abs(y1-y2)            
            c+=delta
        print(c)

Universe(lines)