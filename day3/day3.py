with open("day3/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]

class Schematic():
    def __init__(self,raw):
        self.raw = raw     
        self.parts = [] 

    def generate_parts(self):          
        for y,line in enumerate(self.raw):       
            i=0            
            while i < len(line):
                lenght = 0
                #Search 3 forward, just leave any number bigger than the least expected.
                for x in range(i,i+3):
                    #If it is not a digit, just skip searching
                    if not line[x].isdigit():
                        break
                    lenght+=1

                if line[i].isdigit():                               
                    #Unreadable mumbojumbo
                    part = Part(line[i:i+lenght], x1=i, x2=i+lenght-1 ,y=y,lenght=lenght)
                    self.parts.append(part)            

                #Skip ahead, so we dont waste searching in already found ones or create duplicates.
                i+=1 + lenght

    def validate(self):
        for part in self.parts:
            part.validate(self.raw)

    def count_valid(self) -> int:
        c = 0
        for part in self.parts:
            c+=part.valid
        return c
    
    def count_adjecent_gears(self):
        #Remove all the invalid ones that does not have adjacent gears and sort them
        parts = [part for part in self.parts if part.adjacent_gears]
        parts.sort()

        c = 0
        for i,_ in enumerate(parts):
            if i == len(parts)-1:
                break
            if parts[i].adjacent_gears == parts[i+1].adjacent_gears:
                c+= parts[i].num * parts[i+1].num
        return c


class Part():
    def __init__(self,num,x1,x2,y,lenght):
        self.num = int(num)
        self.x1 = x1
        self.x2 = x2
        self.y = y
        self.len = lenght
        self.adjacent_gears = None

    def validate(self,part_schematic:list) -> True:         
        #All possible combinations, just remove the negatives
        self.combinations = []
        for x in range(self.x1-1, self.x2+2):
            if x <0:
                continue
            for y in range(self.y-1,self.y+2):          
                if y <0:
                    continue      
                self.combinations.append((x,y))
        
        #Anything that is not "." and numeric is valid
        self.valid = False
        for x,y in self.combinations:            
            try:
                if part_schematic[y][x].isdigit() or part_schematic[y][x] == ".":
                    pass
                    #Invalid,
                else:
                    self.valid = self.num
                    #We also save adjacent gears for part 2
                    if part_schematic[y][x] == "*":
                        self.adjacent_gears = (x,y)                    
            except IndexError:
                pass

        return self.valid
    
    def __lt__(self, other): 
        s_x, s_y = self.adjacent_gears
        o_x, s_y = other.adjacent_gears
        if s_x < o_x:
            return True

    def __repr__(self) -> str:
        return str(self.num)
    

schematic = Schematic(lines)
schematic.generate_parts()
schematic.validate()
print(schematic.count_valid())
print(schematic.count_adjecent_gears())

