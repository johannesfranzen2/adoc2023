
with open("day15/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]        
    lines = lines[0].split(",")
    
def hash(v):    
    v*=17
    return v % 256    
    
outer_c = 0
for line in lines:    
    c = 0
    for letter in line:
        c=hash(ord(letter)+c)
    outer_c+=c

print(outer_c)