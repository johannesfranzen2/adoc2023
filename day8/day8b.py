with open("day8/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]    


import re
pattern = re.compile(r'([A-Z0-9]+)\s*=\s*\(([^)]+)\)')

start_points = []
#instructions = "LR"
instructions = "LRRRLRRRLRRLRLRRLRLRRLRRLRLLRRRLRLRLRRRLRRRLRLRLRLLRRLLRRLRRRLLRLRRRLRLRLRRRLLRLRRLRRRLRLRRRLLRLRRLRRRLRRLRRLRLRRLRRRLRLRRRLRRLLRRLRRLRLRRRLRRLRRRLRRRLRLRRLRLRRRLRLRRLRRLRRRLRRRLRRRLLRRLRRRLRLRLRLRRRLRLRLRRLRRRLRRRLRRLRRLLRLRRLLRLRRLRRLLRLLRRRLLRRLLRRLRRLRLRLRRRLLRRLRRRR"
maps = {}
for line in lines:
    match = pattern.search(line)
    if match:
        key = match.group(1)
        values = [value.strip() for value in match.group(2).split(',')]
        maps[key] = values
        if key.endswith("A"):
            print(key)
            start_points.append(key)

#parse all start points

#instru
#move all
#check if the end with Z, repeat
print(maps)
next_start_p = start_points
run = True
moves = 0
while(run):
    
    for instr in instructions:     
        moves+=1     
        start_points = next_start_p
        next_start_p = []
        c = 0       
        #print()
        for point in start_points:        
           # print("Y")
            L,R = maps[point]

            if instr == "R":
                current = R
            else:
                current = L
            next_start_p.append(current)
            #print(current)
            if current.endswith("Z"):                
                c+=1

            if c == len(start_points):
                print("Found target!")
                run = False
                break
            

print(moves)