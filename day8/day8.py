with open("day8/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]    


import re
pattern = re.compile(r'([A-Z]+)\s*=\s*\(([^)]+)\)')


instructions = "LRRRLRRRLRRLRLRRLRLRRLRRLRLLRRRLRLRLRRRLRRRLRLRLRLLRRLLRRLRRRLLRLRRRLRLRLRRRLLRLRRLRRRLRLRRRLLRLRRLRRRLRRLRRLRLRRLRRRLRLRRRLRRLLRRLRRLRLRRRLRRLRRRLRRRLRLRRLRLRRRLRLRRLRRLRRRLRRRLRRRLLRRLRRRLRLRLRLRRRLRLRLRRLRRRLRRRLRRLRRLLRLRRLLRLRRLRRLLRLLRRRLLRRLLRRLRRLRLRLRRRLLRRLRRRR"
maps = {}
for line in lines:
    match = pattern.search(line)
    if match:
        key = match.group(1)
        values = [value.strip() for value in match.group(2).split(',')]
        maps[key] = values

print(maps)

current = "AAA"
target = "ZZZ"
run = True
moves = 0
while(run):
    for instr in instructions:        
        if current == target:
            print("Found target!")
            run = False
            break
        moves+=1
        L,R = maps[current]

        if instr == "R":
            current = R
        else:
            current = L

print(moves)