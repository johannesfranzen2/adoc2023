for i in range(3,10+1):
    import os

    os.mkdir("day"+str(i))
    with open("day"+str(i)+"/input" ,"w+") as f:
        f.read()

    with open("day"+str(i)+"/day"+str(i)+".py" ,"w+") as f:
        f.read()