with open("day7/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]
    #lines = lines[10:20]
    print(lines)

cards = ["A", "K","Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"]
card_weights = {k:v for k,v in zip(cards, range(13,0,-1))}

FIVE_KIND = 7
FOUR_KIND = 6
FULLHOUSE = 5
THREE_KIND = 4
TWO_PAIR = 3
ONE_PAIR = 2
HIGH_CARD = 1

#Fullhouse ska bli four of a kind

class Hand():        
    def __init__(self, raw:str):
        self.raw, self.bid = raw
        self.card_type = None                
        self.__type()  
        print(self.card_type)      

    def __type(self):
        count = len(set(self.raw))        
        joker_count = self.raw.count("J")
        #Set Count:
        #1: 5 of a kind
        #2  4 or fullhouse
        #3  3 of a kind or 2 pair
        #4  1 pair
        #5 #High Card

        if count == 2:
            #Check if 4 or fullhouse
            if "J" in self.raw:
                self.card_type = FIVE_KIND
                return
            for c in cards:
                if self.raw.count(c) == 3:                    
                    self.card_type = FULLHOUSE
                    return            
            self.card_type = FOUR_KIND
            return
        
        if count == 3:
            for c in cards:
                if self.raw.count(c) == 3:
                    if "J" in self.raw:
                        self.card_type = FOUR_KIND
                        return
                    self.card_type = THREE_KIND
                    return
                
            #Can become Fullhouse
            if self.raw.count("J") == 2:
                self.card_type = FOUR_KIND
                return

            if "J" in self.raw:
                self.card_type = FULLHOUSE
                return
            
            self.card_type = TWO_PAIR
            return            
        
        if count == 1:
            #Does not change
            self.card_type = FIVE_KIND
        elif count == 4:
            if "J" in self.raw:
                self.card_type = THREE_KIND
                return
            #become three of a kind?
            self.card_type = ONE_PAIR        
        else:
            #if it contains J its now a ONE_PAIR
            if "J" in self.raw:
                self.card_type = ONE_PAIR
                return
            self.card_type = HIGH_CARD

    def __lt__(self,other):
        if self.card_type == other.card_type:
            #If both cards are the same we need to do second order sort.
            for a,b in zip(self.raw, other.raw):
                if a == b:                    
                    continue
                if card_weights[a] < card_weights[b]:
                    return False
                return True            

        if self.card_type < other.card_type:
            return False
        return True

hands = [Hand(line.split()) for line in lines]
hands.sort(reverse=True)

c = 0
for i, hand in enumerate(hands,start=1):    
    c+= int(hand.bid) * i

print(c)

print("249659411 == TOO LOW")