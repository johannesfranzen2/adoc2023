with open("day5/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]


keys = {"seed-to-soil":"soil-to-fertilizer",
"soil-to-fertilizer":"fertilizer-to-water",
"fertilizer-to-water": "water-to-light",
"water-to-light":"light-to-temperature",
"light-to-temperature":"temperature-to-humidity",
"temperature-to-humidity":"humidity-to-location",
"humidity-to-location":None}


class Map():
    def __init__(self,dest,source,lenght,key):
        self.dest = int(dest)
        self.source = int(source)
        self.lenght = int(lenght)
        self.next_key = keys[key]      
        self.key = key

    def lookup_dest(self,item): # test input of this function
        item = int(item)
        if item not in range(self.source,self.source+self.lenght):
            return False
        
        index = abs(self.source-item)
        #print(f"Found: {key} : {self.dest+index} ")
        return self.dest+index

    def __repr__(self):
        return f"{self.dest} map"

dict_stuff = {}
#filter out "seeds"
dict_stuff["seeds"] = [int(line) for line in lines[:1][0].split() if not ":" in line] 
dict_stuff["seeds2"] = [dict_stuff["seeds"][i:i+2] for i in range(0,len(dict_stuff["seeds"]),2)]
dict_stuff["seeds2"] = [range(start,start+amount-1) for start,amount in dict_stuff["seeds2"]]

#Parse and filter data into dict
lines = [line.split() for line in lines[1:] if not line == ""]
for line in lines:    
    if len(line) == 2:
        key = line[0]
        dict_stuff[key] = []
    else:
        dest, source, lenght = line
        m = Map(dest, source, lenght, key)
        dict_stuff[key].append(m) 
    
print(dict_stuff)

#Now our dict contains lists of Maps
#Recursion?
def solve(seeds_l):
    locations = []
    for seeds in seeds_l:
        for seed in seeds:
            #print(seed)     
            key = "seed-to-soil"
            while(1):
                try:
                    for mapsky in dict_stuff[key]:
                        #Check until we find
                        
                        if mapsky.lookup_dest(seed):                    
                            seed = mapsky.lookup_dest(seed)
                            break                                        

                    #seed is def so keep but increment key
                    key = keys[key] 
                    #print(f"Found: {key} : {seed} ")
                    #print(key)           
                    
                except Exception as e:
                    #print(e)
                    #print(f"Location Found: {seed}")        
                    locations.append(seed)    
                    break
        print("Iteration Done")
    print(f"min: {min(locations)}")

#solve(dict_stuff["seeds"])
solve(dict_stuff["seeds2"])



#Ska man börja kolla efter lägsta location?