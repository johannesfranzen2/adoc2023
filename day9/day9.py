with open("day9/input", "r") as file:    
    lines = [line.rstrip().split() for line in file.readlines()]        

sequences = lines

class Sequence():
    def __init__(self, raw):
        self.raw = raw                
        self.parsed = self.parse(self.raw, [[int(x) for x in raw]])              
        #self.extrapolate()
        

    def parse(self, sequence:list[int], seq_l) -> list:         
        if not sequence:
            return None

        delta = []          
        for i in range(0,len(sequence)-1):      
            delta.append(int(sequence[i+1]) - int(sequence[i]))

        if len(set(delta)) == 1:            
            if delta[0] == 0:
                return None
                
        #Lets recurse
        seq_l.append(delta)
        r = self.parse(delta, seq_l)
        if not r == None:
            seq_l.append(r) 
        return seq_l

    def extrapolate(self):
        #print("Extrapolate")
        #add together all the last numbers   
        num_sum = 0             
        for num in self.parsed:
            #Recursion contains a bug so this except is needed, sadly
            try:                
                num_sum+=num[-1]
            except:
                break
        
        return num_sum

c = 0
for seq in sequences:   
    s = Sequence(seq)
    c+=s.extrapolate()

print(c)
