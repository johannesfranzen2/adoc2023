with open("day6/input", "r") as file:    
    lines = [line.rstrip() for line in file.readlines()]

times = [int(line) for line in lines[0].split() if line.isdigit()]
distances = [int(line) for line in lines[1].split() if line.isdigit()]

print(lines)

c = 1
for time,distance in zip(times,distances):
    local_c = 0
    for i in range(1,time+1):
        if i*(time-i) > distance:
            local_c+=1
    c *= local_c
    local_c = 0

print(c)

import time as dur
time = 44899691
distance = 277113618901768

t = dur.time()
c = 0
for i in range(1,time+1):
    if i*(time-i) > distance:
        c+=1
print(f"Bruteforce: {c} took: {dur.time()-t}S")



